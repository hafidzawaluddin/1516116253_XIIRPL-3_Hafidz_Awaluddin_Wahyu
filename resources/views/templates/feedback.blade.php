@if(session('success'))
	<div class="alert alert-success fade in">
		<button class="close pull-right" type="button" data-dismiss="alert" arial-label="Close"> <span arial-hidden="true"></span> </button>
		{!! session('success') !!}
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger fade in">
		<button class="close pull-right" type="button" data-dismiss="alert" arial-label="Close"> <span arial-hidden="true"></span> </button>
		<p>Perhatian</p>
		{!! session('error') !!}
	</div>
@endif