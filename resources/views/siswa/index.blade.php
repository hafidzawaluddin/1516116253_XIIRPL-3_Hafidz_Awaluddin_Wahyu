@extends('templates/header')

@section('content')

<section>
	<h1>
		DataSiswa
		<small>SMK Negeri 4 Bandung</small>
	</h1>
	<ol>
		<li><a href="{{ url('/') }}"> <i class="fa fa-dashboard"></i> Home </a></li>
		<li class="active">Data Siswa</li>
	</ol>
</section>

<section class="content">
	@include('templates/feedback')
	<div class="box">
		<div class="box-header with-border">
			<a href="{{ url('siswa/add') }}" class="btn btn-success"> <i class="fa fa-plus-circle"></i> Tambah </a>
		</div>
		<div class="box-body">
			<table class="table table-stripped">
				<thead>
					<tr>	
						<td>No</td>
						<td>foto</td>
						<td>Nama Lenkap</td>
						<td>Jenis Kelamin</td>
						<td>Alamat</td>
						<td>No Telp</td>
						<td>Kelas</td>
						<td>Action</td>
					</tr>
				</thead>

				<tbody>
					@foreach ($result as $row)
					<tr>
						<td> {{ !empty($i) ? ++$i : $i = 1 }}</td>
						<td>
							<img src="{{ asset('uploads/'.$row->foto) }}" width="80px" class="img">
						</td>
						<td> {{ @$row->nama_lengkap }}</td>
						<td> {{ @$row->jenis_kelamin }}</td>
						<td> {{ @$row->alamat }}</td>
						<td> {{ @$row->no_telp }}</td>
						<td> {{ @$row->id_kelas }}</td>
						<td>
							<a href="{{ url("siswa/$row->nis/edit") }}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i></a>
							<form action="{{ url("siswa/$row->nis/delete") }}" method="post" style="display: inline;">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
@endsection